package com.lihaishan.dahuatech.dao;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class BookDao {
    private String flag="1";

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "BookDao{" +
                "flag='" + flag + '\'' +
                '}';
    }
}
