package com.lihaishan.dahuatech.filter;

import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.stereotype.Repository;
import sun.net.ResourceManager;

import java.io.IOException;

public class MyTypeFilter implements TypeFilter {
    /**
     *
     * @param metadataReader:读取到当前正在扫描的类的信息
     * @param metadataReaderFactory：可以获取到任何类的信息
     * @return
     * @throws IOException
     */

    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
        /**获取当前注解类的信息 */
        AnnotatedTypeMetadata annotatedTypeMetadata = metadataReader.getAnnotationMetadata();
        /**获取当前正在扫描的类的信息 */
        ClassMetadata classMetadata =metadataReader.getClassMetadata();
        /**获取当前类资源的信息；例如：路径 */
        Resource resource = metadataReader.getResource();
        /**按类扫描 */
//        String className = classMetadata.getClassName();
//        System.out.println(className);

//        if (className.contains("er")){
//            return true;
//        }
        /**按注解扫描*/
        MergedAnnotations mergedAnnotations = annotatedTypeMetadata.getAnnotations();
        System.out.println(mergedAnnotations);
        return true;
    }
}
