package com.lihaishan.dahuatech.test;

import com.lihaishan.dahuatech.config.PropertiesConfig;
import com.lihaishan.dahuatech.pojo.Person;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

public class PropertiesTest {
    @Test
    public void propertiesTest(){
        ApplicationContext ac = new AnnotationConfigApplicationContext(PropertiesConfig.class);
        String[] s = ac.getBeanDefinitionNames();
        for (String s1 : s){
            System.out.println(s1);
        }
        Person person = (Person) ac.getBean("person");
        System.out.println(person);
        Environment environment = ac.getEnvironment();
        environment.getProperty("NickName");
    }
}
