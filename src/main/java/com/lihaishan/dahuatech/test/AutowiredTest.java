package com.lihaishan.dahuatech.test;

import com.lihaishan.dahuatech.config.AutowiredConfig;
import com.lihaishan.dahuatech.service.BookService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AutowiredTest {
    @Test
    public void autowiredTest(){
        ApplicationContext ac= new AnnotationConfigApplicationContext(AutowiredConfig.class);
        BookService bookService = ac.getBean(BookService.class);
        String[] s = ac.getBeanDefinitionNames();
        for (String s1:s){
            System.out.println(s1);
        }
        System.out.println(bookService);



    }
}
