package com.lihaishan.dahuatech.test;

import com.lihaishan.dahuatech.config.MainConfig;
import com.lihaishan.dahuatech.pojo.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainTest {
    public static void main(String[] args) {
//        ApplicationContext ac = new ClassPathXmlApplicationContext("beans.xml");
//        Person p = (Person) ac.getBean(Person.class);
//        System.out.println(p);

        ApplicationContext acc = new AnnotationConfigApplicationContext(MainConfig.class);
        Person p=acc.getBean(Person.class);
        System.out.println(p);

        String[] s =acc.getBeanNamesForType(Person.class);
        for (String s1:s){
            System.out.println(s1);
        }
    }
}
