package com.lihaishan.dahuatech.test;

import com.lihaishan.dahuatech.config.ProfileConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sql.DataSource;

/**
 * 切换环境：
 * 1、动态参数 :-Dspring.profiles.active=dev切换开发环境
 * 2、代码方式:使用无参构造的AnnotationConfigApplicationContext，手动注册手动刷新
 */
public class DataSourceTest {

    @Test
    public void dataSourceTest(){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ProfileConfig.class);
        String[] s = applicationContext.getBeanNamesForType(DataSource.class);
        for (String s1 :s){
            System.out.println(s1);
        }

    }

    @Test
    public void dataSourceTest2(){
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        /**
         * 1、创建一个applicationContext对象
         *
         * 2、设置一个需要激活的环境
         *
         * 3、注册猪配置类
         */
        applicationContext.getEnvironment().setActiveProfiles("test","dev");
        applicationContext.register(ProfileConfig.class);
        applicationContext.refresh();
        String[] s = applicationContext.getBeanNamesForType(DataSource.class);
        for (String s1 :s){
            System.out.println(s1);
        }

    }
}
