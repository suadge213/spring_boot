package com.lihaishan.dahuatech.test;

import com.lihaishan.dahuatech.config.BeanLifeCycleConfig;
import com.lihaishan.dahuatech.pojo.Car;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class LifyCycleTest {

    @Test
    /**
     * 容器创建的时候初始化。
     * 调用ac.close.调用销毁
     */
    public void lifeCycleTest(){
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(BeanLifeCycleConfig.class);
        System.out.println("容器创建完毕");
//        Car car = ac.getBean(Car.class);
        ac.close();

    }
}
