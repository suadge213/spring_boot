package com.lihaishan.dahuatech.test;

import com.lihaishan.dahuatech.condition.ColorFactoryBean;
import com.lihaishan.dahuatech.config.IntanceBeanConfig;
import com.lihaishan.dahuatech.config.MainConfig;
import com.lihaishan.dahuatech.pojo.Color;
import com.lihaishan.dahuatech.pojo.Person;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

public class IOCTest {
    ApplicationContext ac = new AnnotationConfigApplicationContext(IntanceBeanConfig.class);

    /**
     * BeanFactory:测试类
     */
    @Test
    public void BeanFactoryTest() throws Exception {
//        ColorFactoryBean colorFactoryBean = new ColorFactoryBean();
//        Color color = (Color) colorFactoryBean.getObject();
        /**
         * return:Color实例
         */
        Object beans = ac.getBean("colorFactoryBean");
        System.out.println("bean的类型"+beans.getClass());
//        String[] s = ac.getBeanDefinitionNames();
//        for (String s1:s){
//            System.out.println(s1);
//        }
    }


    //
    @Test
    public void registerIOCTest(){
        printBeans(ac);
    }
    public void printBeans(ApplicationContext ac ){
        String[] s = ac.getBeanDefinitionNames();
        for (String s1:s){
            System.out.println(s1);
        }
    }




    @SuppressWarnings("resource")
    @Test
    public void annotionTest2() {
        ApplicationContext ac = new AnnotationConfigApplicationContext(IntanceBeanConfig.class);
//        String[] s = ac.getBeanDefinitionNames();
//        for (String s1 : s){
//            System.out.println(s1);
//        }
//
//        Person person = ac.getBean(Person.class);
//        Person person1 = ac.getBean(Person.class);
//        System.out.println(person==person1);
        /**
         * 多实例，prototype 创建实例过程
         *
         */
//        System.out.println("ioc创建完毕");
//        Person person = ac.getBean(Person.class);
//        Person person2 = ac.getBean(Person.class);
        /**
         * lazy：懒加载，容器启动不会创建，只有getBean时才会
         * 因为是单例模式，所以第二次获取还是第一次的对象。
         */

//        System.out.println("ioc创建完毕");
//        Person person = ac.getBean(Person.class);
//        Person person2 = ac.getBean(Person.class);
//        System.out.println(person==person2);
        /**
         * @Contional
         */
        //获取IOC容器的环境
//        Environment environment = ac.getEnvironment();
        //获取操作系统的名称
//        String property = environment.getProperty("os.name");
        //output:windows 10
//        System.out.println(property);


        //获取对象名
//         String[] s =ac.getBeanNamesForType(Person.class);
//
//         for (String s1 : s ){
//             System.out.println(s1);
//         }
//         Person p = ac.getBean(Person.class);
//        System.out.println(p);
//         //获取类
//        Map<String,Person> map = ac.getBeansOfType(Person.class);
//         for (Map.Entry entry : map.entrySet()){
//             System.out.println(entry.getKey()+"______"+entry.getValue());
//         }

    }

    @SuppressWarnings("resource")
    @Test
    public void annotionTest() {
        ApplicationContext ac = new AnnotationConfigApplicationContext(MainConfig.class);
        String[] s = ac.getBeanDefinitionNames();
        for (String s1 : s) {
            System.out.println(s1);
        }
        System.out.println();

    }


}
