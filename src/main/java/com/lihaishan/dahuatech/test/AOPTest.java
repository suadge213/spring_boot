package com.lihaishan.dahuatech.test;

import com.lihaishan.dahuatech.aop.Calculator;
import com.lihaishan.dahuatech.config.AOPConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AOPTest {
    @Test
    public void aopTest(){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AOPConfig.class);
        Calculator calculator = applicationContext.getBean(Calculator.class);

        int a =calculator.div(10,2);
    }
}
