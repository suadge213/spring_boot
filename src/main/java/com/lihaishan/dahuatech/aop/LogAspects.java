package com.lihaishan.dahuatech.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;

import java.util.Arrays;

@Aspect
public class LogAspects {
    //1、本类引用、
    //2、其他类引用
    @Pointcut("execution(public int com.lihaishan.dahuatech.aop.Calculator.*(..))")
    public void pointCut(){

    }

    @Before("pointCut()")
    public  void logStartCustom(JoinPoint joinPoint){
        Object[] obj = joinPoint.getArgs();
        System.out.println(""+joinPoint.getSignature().getName()+"触发运行"+ Arrays.asList(obj));
    }
    @After("pointCut()")
    public  void logStopCustom(){
        System.out.println("结束运行");
    }
    @AfterReturning(value = "pointCut()",returning = "result")
    public  void logTrueCustom(Object result){
        System.out.println("正常返回{}"+result);
    }

    @AfterThrowing(value = "pointCut()",throwing = "exception")
    public  void logExceptionCustom(Exception exception){
        System.out.println("一场返回{}");
    }

}
