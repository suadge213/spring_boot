package com.lihaishan.dahuatech.pojo;

public class Boss {
    private Car cat;

    @Override
    public String toString() {
        return "Boss{" +
                "cat=" + cat +
                '}';
    }

    public Car getCat() {

        return cat;
    }

    public void setCat(Car cat) {
        this.cat = cat;
    }
}
