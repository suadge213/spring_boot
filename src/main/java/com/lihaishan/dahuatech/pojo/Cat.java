package com.lihaishan.dahuatech.pojo;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class Cat implements InitializingBean, DisposableBean {
    public Cat(){
        System.out.println("cat construct");
    }
    @Override
    public void destroy() throws Exception {
        System.out.println("destory cat bean");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("init Cat bean");
    }
}
