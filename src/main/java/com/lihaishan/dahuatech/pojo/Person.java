package com.lihaishan.dahuatech.pojo;

import org.springframework.beans.factory.annotation.Value;

public class Person {
    @Value("李海山")
    private   String name ;
    @Value("#{18-4}")
    private Integer age;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Value("${NickName}")
    private  String nickName;

    public Person() {

    }
    public Person(String name,Integer age){
        this.name=name;
        this.age=age;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", nickName='" + nickName + '\'' +
                '}';
    }
}
