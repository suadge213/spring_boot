package com.lihaishan.dahuatech.pojo;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Bike {
    public Bike() {
        System.out.println("Bike construct");
    }

    //对象创建并赋值之后调用
    @PostConstruct
    public void init() {
        System.out.println("Dog-------constarctor");
    }


    //对象创建并赋值之后调用
    @PreDestroy
    public void destory() {
        System.out.println("Dog-------clean");
    }
}
