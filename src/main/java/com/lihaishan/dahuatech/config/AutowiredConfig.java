package com.lihaishan.dahuatech.config;

import com.lihaishan.dahuatech.dao.BookDao;
import com.lihaishan.dahuatech.service.BookService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 *
 * 自动装配：
 *      Spring利用依赖注入（DI），完成对IOC容器中各个组件依赖关系的赋值
 *
 * 1、@Autowired,自动注入：
 *     默认有限按照类型去容器中找相应的组件
 *     如果注册了多个相同类型的组件，再讲属性的名称作为组件的id去容器中查找
 *     @Qualifier 使用该标签可以明确指定装配的组件
 *     如果容器中没有组件会泡NoSuchBeanException，自动装配一定能在容器中找到这个组件，否则报错
 *      *     @Autowired(required = false)如果没有这个组件，会注入dao=null
 *  *     @Primary 让容器装配的时候首选
 * 2、Spring还支持@Resource（JSR250）和@Inject(JSR330)
 *      @Resource:和Autowired一样可以实现自动装配功能；默认是使用名称进行装配
 *      但是他们没有支持@Autowired(required = false)和Primary功能
 * 3、@Inject注解
 * 4、@Autowired可以注解方法，构造器，属性等
 * 5、自定义组件想要使用Spring底层的一些组件
 *      自定义组件实现####aware ：在创建兑现给的时候，会调用接口规定的方法注入相关组件
 *
 *
 *
 *
 *

 *   BookService{
 *          @Autowired
 *          BookDao
 *   }
 */
@Configuration
@ComponentScan({"com.lihaishan.dahuatech.controller","com.lihaishan.dahuatech.service","com.lihaishan.dahuatech.dao","com.lihaishan.dahuatech.pojo"})
public class AutowiredConfig {
//    @Primary
    @Bean("bookService2")
    public BookDao bookService(){
        BookDao bookDao = new BookDao();
        bookDao.setFlag("2");
        return bookDao;
    }

}
