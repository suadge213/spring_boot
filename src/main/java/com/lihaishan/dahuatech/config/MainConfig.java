package com.lihaishan.dahuatech.config;

import com.lihaishan.dahuatech.filter.MyTypeFilter;
import com.lihaishan.dahuatech.pojo.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(value = "com.lihaishan.dahuatech", includeFilters = {
//        @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = Controller.class),
//        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,classes = BookService.class),
        @ComponentScan.Filter(type = FilterType.CUSTOM, classes = MyTypeFilter.class)
}, useDefaultFilters = false)
public class MainConfig {
    /**
     * 给容器注册一个bean,类型为返回类型，id默认用方法名作为id
     */
    @Bean("李四")
    public Person person1() {
        return new Person("李四", 20);
    }
}
