package com.lihaishan.dahuatech.config;

import com.lihaishan.dahuatech.aop.Calculator;
import com.lihaishan.dahuatech.aop.LogAspects;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * AOP：
 * 面向程序编程期间，动态的将某段代码切入到指定方法指定位置进行的编码方式、
 * 底层：动态代码
 * 1、导入AOP模块：Spring AOP（Spring aspect）
 * 2、定义一个业务逻辑类：在业务逻辑运行的时候，将日志进行打印，在方法运行之前，运行至后，异常都进行打印
 * 3、定义一个日志切面类。
 *     通知方法：
 *          前置通知：在目标方法执行前运行  @Before
 *          后置通知：在目标方法执行后运行，无论方法正常结束还是异常结束都会调用     @After
 *          返回通知：在目标方法正常返回后运行  @AfterReturn
 *          异常通知：在目标方法异常返回后运行      @AfterThrowing
 *          环绕通知：动态代理，手动推进目标方法运行(joinPoint.proceed()) @Around
 * 4、给切面类的目标方法标注何时何地运行
 * 5、将切面类和目标业务逻辑类都加入到容器中
 * 6、必须告诉Spring哪个类是切面类@Aespect
 * 7、给配置类加上@EnableAspectJAutoProxy，
 * 8、JoinPotin参数，出现在参数表的第一位
 *
 *
 * 三、
 * 1、将业务逻辑切面和切面类都加入到容器中，告诉Spring哪个是切面
 * 2、在切面类上的每个通知方法标注注解解释，告诉spring如何运行
 * 3、开启基于注解的AOP模式
 *
 *
 * 四、AOP逻辑（给容器中注册了什么组件，这个组件什么时候工作，以及工作时候的功能）
 * 1、@EnableAspectJAutoProxy：
 * @Import({AspectJAutoProxyRegistrar.class})
 * 2、AnnotationAwareAspectJAutoProxyCreator:
 *      -》AspectJAwareAdvisorAutoProxyCreator
 *          -》AbstractAdvisorAutoProxyCreator
 *              -》AbstractAutoProxyCreator
 *               -》ProxyProcessorSupport && SmartInstantiationAwareBeanPostProcessor, BeanFactoryAware
 *
 *
 *
 */
@EnableAspectJAutoProxy
@Configuration
public class AOPConfig {

    @Bean
    public Calculator getCalculator(){
        return new Calculator();
    }
    @Bean
    public LogAspects logAspects(){
        return  new LogAspects();
    }
}
