package com.lihaishan.dahuatech.config;

import com.lihaishan.dahuatech.pojo.Car;
import com.lihaishan.dahuatech.pojo.Cat;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * bean的生命周期：
 * bean的创建----初始化----小王
 * 容器管理bean的生命周期
 * 我们可以自定义初始化和销毁方法：容器在bean进行到当前生命周期的时候来调用我们自定义的初始化和销毁方法
 * 构造（容器创建）
 *  单实例：在容器启动的时候创建对象
 *  多实例：在每次获取的时候创建对象
 * 1、指定初始化和销毁方法：
 *      指定init-method和detroy-method
 *      初始化方法:对象创建完成，并赋值好，执行初始化
 *      销毁：容器关闭
 *      单实例bean：容器关闭
 *      多实例bean：容器不会管理这个bean
 *  2、实现InitializingBean, DisposableBean 接口调用init 和detory方法
 *  3、实现PostConstruct:在bean创建完成，并且属性赋值完成，
 *      #PreDestory：在容器销毁bean之前，通知进行清理工作。
 *  4、BeanPostProcessor:bean的后置处理器:interface
 *     作用:在bean初始化前执行一些工作
 *     postProcessBeforeInitialization：在初始化之前做一些工作
 *     postProcessAfterInitialization:在初始化之后做一些工作
 *  Spring底层对BeanPostProcessor的使用：
 *
 *
 *
 *
 */

@ComponentScan({"com.lihaishan.dahuatech.pojo","com.lihaishan.dahuatech.condition"})
@Configuration
public class BeanLifeCycleConfig {
    @Scope("prototype")
//    @Bean(initMethod = "init",destroyMethod = "detory")
    @Bean
    public Car car(){
        return new Car();
    }
}
