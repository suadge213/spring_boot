package com.lihaishan.dahuatech.config;

import com.lihaishan.dahuatech.pojo.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @Value注解赋值
 * 1、基本数值
 * 2、可以写SpEL，#{}  例如#{20-2}
 * 3、可以用${}，取出配置文件中的值
 */
@PropertySource(value = {"classpath:/person.properties"})
@Configuration
public class PropertiesConfig {
    @Bean
    public Person person(){
        return new Person();
    }
}
