package com.lihaishan.dahuatech.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.StringValueResolver;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * Profile:
 * Spring提供的根据当前环境，动态的激活和切换一系列组件的功能；
 * 开发环境/测试环境/生产环境
 * 数据源：A.B.C
 *@Profile():指定组件在哪个环境下才能被注册到容器中，不指定，在任意环境下都能注册
 * 1)加了环境表示的bean，只有这个环境被激活的时候才能注册到容器中
 * 2）
 */
//@PropertySource("classpath:/dbSource.properties"

@Configuration
public class ProfileConfig implements EmbeddedValueResolverAware {
    @Value("root")
    private String userName;
    private StringValueResolver stringValueResolver;
    @Bean("testDataSource")
    @Profile("test")
 public DataSource dataSourceTest(@Value("") String password) throws PropertyVetoException {
     ComboPooledDataSource dataSource = new ComboPooledDataSource();
     dataSource.setUser("root");
     dataSource.setPassword(password);
     dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test");
     dataSource.setDriverClass("com.mysql.jdbc.Driver");

     return dataSource;
 }
    @Profile("dev")
    @Bean("devDataSource")
    public DataSource dataSourceDev(@Value("${password}") String password) throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser("root");
        dataSource.setPassword(password);
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/dev");
        dataSource.setDriverClass("com.mysql.jdbc.Driver");

        return dataSource;
    }
    @Profile("work")
    @Bean("worktDataSource")
    public DataSource dataSourceWork(@Value("${password}") String password) throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser("root");
        dataSource.setPassword(password);
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/work");
        String driverClass = stringValueResolver.resolveStringValue("com.mysql.jdbc.Driver");
        dataSource.setDriverClass(driverClass);

        return dataSource;
    }

    @Override
    public void setEmbeddedValueResolver(StringValueResolver stringValueResolver) {
            this.stringValueResolver=stringValueResolver;
    }
}
