package com.lihaishan.dahuatech.config;

import com.lihaishan.dahuatech.condition.*;
import com.lihaishan.dahuatech.pojo.Color;
import com.lihaishan.dahuatech.pojo.Person;
import com.lihaishan.dahuatech.pojo.Red;
import org.springframework.context.annotation.*;


@Configuration
//@Import({Color.class, Red.class, MyImportSelector.class, MyImportBeanDefinitionRegistrar.class})
public class IntanceBeanConfig {
    //默认是单实例的

    /**
     * Scope:ConfigurableBeanFactory
     * param:
     * prototype:多实例：IOC启动过程不会获取对象，每次获取对象的时候才会获取。每次获取都会调用一次
     * singleton：单实例:ioc容器启动时会创建实例放入容器中。以后每次获取都是直接从容器中获取
     * request：一次请求一个实例
     * session：同一个事物一个实例
     * @return
     */
//    @Scope("prototype")
//    @Bean("赵晨时")
//    public Person person(){
//        System.out.println("给容器中创建person类");
//
//        return new Person("赵晨是",25);
//    }

    /**
     * 懒加载：
     * 容器启动时创建对象，只有第一次获取bean时才会创建对象，进行初始化
     *
     * @return
     */
//    @Lazy
//    @Bean("赵晨时")
//    public Person person() {
//        System.out.println("给容器中创建person类");
//
//        return new Person("赵晨是", 25);
    /**
     * @Conditional ： 按照一定的条件进行判断，满足一定的条件给容器中注册bean
     * @Param: {Class Condition}
     *  如果是Windows输出老婆，如果是linux输出我
     */

//    @Conditional(LinuxCondition.class)
//    @Bean("俞姣君")
//    public Person person(){
//        System.out.println("俞姣君");
//        return new Person("俞姣君",24);
//    }
//    @Conditional(WindowsCondition.class)
//    @Bean("李海山")
//    public Person person2(){
//        System.out.println("李海山");
//        return new Person("李海山",24);
//    }
    /**
     * 给容器中注册组件
     * 1、包扫描+组件标注注解（@Component、@Controller、@Service、@Repository） 适用于：自己写的类
     * 2、@Bean[导入第三方包]
     *  例如：
     *  //    public Person person2(){
     * //        System.out.println("李海山");
     * //        return new Person("李海山",24);
     * //    }
     * 3、@Import
     * a、直接使用@import；容器中就会自动注入这个组件
     * b、ImportSelector（interface）:返回需要导入的组件的全类名数组。
     * c、ImportBeanDefinitionRegistrar
     * 4、使用Spring提供的：FactoryBean（工厂Bean）
     *    通过工程Bean可以获得Color实例
     *    获取工厂Bean本身需要在前面+&
     */
    @Bean
    public ColorFactoryBean colorFactoryBean(){
        return new ColorFactoryBean();
    }

}
