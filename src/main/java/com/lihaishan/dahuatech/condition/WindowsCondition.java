package com.lihaishan.dahuatech.condition;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class WindowsCondition implements Condition {

    @Override
    /**
     * @Param:
     * ConditionContext:判断条件能使用的上下文
     * AnnotatedTypeMetadata:注释信息
     */
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        //TODO 是否windows系统
        //能获取到IOC当前的bean工厂
        ConfigurableListableBeanFactory configurableListableBeanFactory = conditionContext.getBeanFactory();
        //获取类加载器
        ClassLoader classLoader = conditionContext.getClassLoader();
        //获取环境信息
        Environment environment = conditionContext.getEnvironment();
        //获取Bean定义的注册类
        BeanDefinitionRegistry beanDefinitionRegistry = conditionContext.getRegistry();
        /**
         * 判断容器中bean的注册情况
         */
        boolean personContain = beanDefinitionRegistry.containsBeanDefinition("李海山");
        if (personContain){
            return true;
        }

//        String property = environment.getProperty("os.name");
//        if (property.contains("Windows")){
//            return true;
//
//        }

        return false;
    }
}
