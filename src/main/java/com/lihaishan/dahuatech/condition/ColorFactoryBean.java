package com.lihaishan.dahuatech.condition;

import com.lihaishan.dahuatech.pojo.Color;
import org.springframework.beans.factory.FactoryBean;

/**
 * 创建一个工厂bean
 */
public class ColorFactoryBean implements FactoryBean {
    @Override
    public Object getObject() throws Exception {
        System.out.println("get Color Object");
        return new Color();
    }

    @Override
    public Class<?> getObjectType() {
        return Color.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
