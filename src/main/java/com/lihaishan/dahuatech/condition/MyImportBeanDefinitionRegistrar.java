package com.lihaishan.dahuatech.condition;


import com.lihaishan.dahuatech.pojo.Black;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    /**
     *
     * @param importingClassMetadata:当前累的注解信息
     * @param registry：BeanDefinition注册类
     *  吧所有需要添加到容器中的bean：可以调用BeanDefinitionRegistry的方法，自定义注册进来
     */
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        boolean containRed = registry.containsBeanDefinition("com.lihaishan.dahuatech.pojo.Red");
        boolean containBlue = registry.containsBeanDefinition("com.lihaishan.dahuatech.pojo.Blue");
        if (containBlue&&containRed){
            //指定bean的定义信息
            RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(Black.class);
            //指定bean名
            registry.registerBeanDefinition("black", rootBeanDefinition);
        }


    }
}
