package com.lihaishan.dahuatech.condition;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * 后置处理器，在初始化前后进行处理
 */
@Component
public class MyBeanPostProcessor implements BeanPostProcessor {

    public Object postProcessBeforeInitialization(Object bean, String beanName){
        System.out.println("    public Object postProcessBeforeInitialization(Object bean, String beanName){\n"+beanName+"--------"+bean);
        return bean;
    }
    public Object postProcessAfterInitialization(Object bean, String beanName){
        System.out.println("    public Object postProcessBeforeInitialization(Object bean, String beanName){\n"+beanName+"--------"+bean);
        return bean;
    }

}
