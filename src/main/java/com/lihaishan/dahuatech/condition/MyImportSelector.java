package com.lihaishan.dahuatech.condition;

import com.lihaishan.dahuatech.pojo.Color;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.function.Predicate;

public class MyImportSelector implements ImportSelector {
    /**
     *
     * @param annotationMetadata:当前标注@Import注解类的所有的注解信息
     * @return
     */
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        //annotationMetadata.get

        return new String[]{"com.lihaishan.dahuatech.pojo.Blue","com.lihaishan.dahuatech.pojo.Yellow"};
    }

    @Override
    public Predicate<String> getExclusionFilter() {
        return null;
    }
}
